package com.example.simpletablayout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPager2Adapter(
    fragmentActivity: FragmentActivity,
    private var listfragment: ArrayList<Fragment>,
) : FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = listfragment.size

    override fun createFragment(position: Int): Fragment {
        return listfragment[position]
    }
}