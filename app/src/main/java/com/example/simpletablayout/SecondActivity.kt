package com.example.simpletablayout

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.simpletablayout.databinding.ActivitySecondBinding
import com.google.android.material.tabs.TabLayoutMediator

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragment = arrayListOf(FirstFragment.newInstance("Ini Simple Args"), SecondFragment())
        val titleFragment = arrayListOf("Tentang", "Materi Kelas")
        val viewPager2AdapterAdapter = ViewPager2Adapter(this, fragment)
        binding.viewPager2.adapter = viewPager2AdapterAdapter
        TabLayoutMediator(binding.tabLayout, binding.viewPager2) { tab, position ->
            tab.text = titleFragment[position]
        }.attach()
    }
}