package com.example.simpletablayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.simpletablayout.databinding.ActivityMainBinding

//DETAIL ACTIVITY / DETAIL FRAGMENT
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fragment = arrayListOf(FirstFragment.newInstance("Ini Simple Args"), SecondFragment())
        val titleFragment = arrayListOf("Tentang", "Materi Kelas")
        val viewPager1AdapterAdapter = ViewPager1Adapter(supportFragmentManager, fragment, titleFragment)
        binding.viewPager.adapter = viewPager1AdapterAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }
}