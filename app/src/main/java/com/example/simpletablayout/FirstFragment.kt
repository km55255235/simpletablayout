package com.example.simpletablayout

import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.example.simpletablayout.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Fetch Bundle
        val simpleArgs = arguments?.getString("ARGS_ID")
        binding?.simpleArgsText?.text = simpleArgs
    }

    companion object {
        fun newInstance(simpleArgs: String): Fragment {
            val args = Bundle()
            args.putString("ARGS_ID", simpleArgs)

            val fragment = FirstFragment()
            fragment.arguments = args
            return fragment
        }
    }

}