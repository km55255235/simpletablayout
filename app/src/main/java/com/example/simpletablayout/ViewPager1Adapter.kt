package com.example.simpletablayout

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewPager1Adapter(
    fm: FragmentManager,
    private var listfragment: ArrayList<Fragment>,
    var title: ArrayList<String>
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    var pos: Int = 0

    override fun getItem(position: Int): Fragment {
        return listfragment[position]
    }

    override fun getCount(): Int {
        return listfragment.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return title[position]
    }
}
